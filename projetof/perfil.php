<?php
  session_start();
 //if (($_SESSION['logado'] && $_SESSION['tipoUser'] == 0) || (!$_SESSION['logado']))
  if((!$_SESSION["logado"] || $_SESSION["tipoUser"]!=1))
      header("Location:login2.php");
    header("Content-Type: text/html; charset=utf-8",true);

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title><?php echo "Bem Vindo ".ucfirst($_SESSION["nome"])?></title>
  <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
  <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }

      ul{
        list-style:none;
        list-style-type: none;
        text-decoration: none;
        margin-left: 0px;
      }
      #centro{
        float: right;
      }
      .inp{
        text-align: center;

      }
      .inp input{
        margin: 12px;
      }
      #logo{
        width: 20px;
      }
    </style>

  <link href="bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
   
  <script src="js/jquery-latest.js"></script>
  
 
  <script src="bootstrap/js/bootstrap.min.js"></script>

</head>
<body>

  <?php include "functionsPDO.php";?>

  

    <div class="container">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div class="hero-unit">
        <div class="navbar">
          <div class="navbar-inner">
            <div class="container">
 
              
 
      <!-- Tenha certeza de deixar a marca se você quer que ela seja mostrada -->
            <div class="brand"><img src="img/ifpb-logo.png" id="logo"></div>
 
      <!-- Tudo que você queira escondido em 940px ou menos, coloque aqui -->
      <div class="nav-collapse collapse">
        <ul class="nav">
          <li>
            <a href="principal.php">Início</a>
          </li>
          <li><a href="listar_documentos.php">Buscar Documento</a></li>
          <li class="active"><a href="perfil2.php">Perfil</a></li>
          <li><a href="sair2.php">Sair</a></li>
        </ul>
      </div>
 
    </div>
  </div>
</div>
      </div>
      <div class="container">
        <ul>
        
          <li><div id="centro"><button type="button" data-toggle="modal" data-target="#myModal">Editar Informações</button></div></li>
           
        </ul>
  </div>
  <br>
<div class="container">
        <ul>
         
          <li><div class="alert alert-info">Nome: nome do usuario</div></li>
          <li><div class="alert alert-info">Email: usuario@gmail.com</div></li>
          <li><div class="alert alert-info">Senha: senhaUsuario</div></li>
          
        </ul>
  </div>

  <!-- Modal Informações-->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel" class='inp'>Editar informações básicas</h3>
  </div>
  <form method='post' action>
  <div class="modal-body">
    <?php

      $dados = getUsuarioPorId($_SESSION['id']);

      foreach ($dados as $key) {
        echo "<div class='container-fluid inp' ><input type='text' placeholder='Nome' required='required' value='{$key->nome}'>";
        echo "<input type='text' placeholder='Email' required='required' value='email do usuario'>";
        echo "<input type='password' placeholder='Senha' required='required'>";
        echo "<input type='password' placeholder='Confirma Senha' required='required' >";
      }

      
    ?>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
    <button class="btn btn-primary">Salvar mudanças</button>
  </div>
  </form>
</div>
 
      <hr>

      <footer>
        <p>&copy; IFPB - João Pessoa</p>
      </footer>

    </div> <!-- /container -->
  
</body>
</html>