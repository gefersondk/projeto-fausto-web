<?php

header("Content-Type: text/html; charset=utf-8",true);
function dados_conexao($file_name){

	$dados =  array();
	$i = 0;
	if (file_exists($file_name)) {
    	$xml = simplexml_load_file($file_name);
    	foreach ($xml as $key => $value) {
    		$dados[$i] = $value;
    		$i++;
    	}
    	return $dados;
 
	} else {
    	exit('Falha ao abrir: '.$file_name.'.xml.');
	}	

}

function conectarComPdo(){
    //$dsn = "mysql:host=".HOST.";dbname=".BD;
    $dsn = "pgsql:host=".HOST.";dbname=".BD;
    try{
        $conectar = new PDO($dsn,USER,PASS);
        $conectar->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        return $conectar;
    }catch(PDOException $e){
        echo "erro ao se conectar com a base de dados";
		echo  $e->getMessage( );
		
    }//j
}


$con = dados_conexao("dadosBanco.xml");
define('HOST',$con[0]);
define('USER',$con[1]);
define('PASS',$con[2]);
define('BD',$con[3]);
conectarComPdo();


?>