<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<script src="js/jquery-1.11.0.min.js"></script>
	<script src="js/lightbox.min.js"></script>
	<link href="css/lightbox.css" rel="stylesheet" />
	
	<script>
		$(document).ready(function() {
		$('#content').load('lightbox.html', 
			function(){
				$('#gallery a').lightBox();
			}
		);
	});
	</script>

</head>
<body>
	<a href="img/a.jpg" data-lightbox="image-1" data-title="My caption">Image #1</a>
</body>
</html>