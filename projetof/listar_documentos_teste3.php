<?php
  session_start();
  if (!$_SESSION['logado'])
      header("Location:login2.php");
    header("Content-Type: text/html; charset=utf-8",true);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title><?php echo "Bem Vindo ".ucfirst($_SESSION["nome"])?></title>
  <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
  <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
    </style>
  <link href="bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
  <script type="text/javascript">
        function busca(){
          
            var xmlhttp;
            var nome = document.getElementById('nome').value;
            var cpf = document.getElementById('cpf').value;
            var matricula = document.getElementById('matricula').value;
            var rg = document.getElementById('rg').value;
            

          
            if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp=new XMLHttpRequest();
            }else{// code for IE6, IE5
                xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange=function(){
                if (xmlhttp.readyState==4 && xmlhttp.status==200){
                    document.getElementById("dados").innerHTML=xmlhttp.responseText;
                    

                }
            }
            xmlhttp.open("GET","busca3.php?nome="+nome+"&matricula="+matricula+"&cpf="+cpf+"&rg="+rg,true);
            xmlhttp.send();

        }
    </script>

  </style>
    <style type="text/css">
      @import "media/css/jquery.dataTables.css";
    </style>
  <script src="media/js/jquery.js"></script>
    <script src="media/js/jquery.dataTables.js"></script>
    
    <script>
      $(document).ready(function(){
        $("#tabela1").dataTable(
          {
                "language": {
                    "url": "media/language/br.txt"
                }
            }
        );
      });
    </script>
</head>
<body>

  

    <div class="container">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div class="hero-unit">
        <div class="navbar">
          <div class="navbar-inner">
            <div class="container">
 
              
 
      <!-- Tenha certeza de deixar a marca se você quer que ela seja mostrada -->
            <a class="brand" href="#">Nome do projeto</a>
 
      <!-- Tudo que você queira escondido em 940px ou menos, coloque aqui -->
      <div class="nav-collapse collapse">
        <ul class="nav">
          <li>
            <a href="principal_admin2.php">Início</a>
          </li>
          <li><a href="gerenciar_usuarios2.php">Gerenciar Usuarios</a></li>
          <li class="active"><a href="listar_documentos_teste2.php">Buscar Documento</a></li>
          <li><a href="perfil2.php">Perfil</a></li>
          <li><a href="sair2.php">Sair</a></li>
        </ul>
      </div>
 
    </div>
  </div>
</div>
      </div>

     <div class="form-inline" role="form" id="pesquisa">
            
                
                    <!--onkeyup="busca(this.value) pra chamar o metodo ajax-->
                    <input type="text" class="input-medium" id="nome" placeholder="Nome" name="nome">
             
                 
                    <input type="text" class="input-medium" id="matricula" placeholder="Matrícula" name="matricula">
             
                    <input type="text" class="input-medium" id="rg" placeholder="Rg" name="rg">
           
                    <input type="text" class="input-medium" id="cpf" placeholder="Cpf" name="cpf">
           
         
                    <input type="text" class="input-medium" id="nomeMae" placeholder="Nome da Mãe" name="nomeMae">
                    <!--class="form-control"-->
                    <input type="text" class="input-medium" id="nomePai" placeholder="Nome do Pai" name="nomePai">
      
                <button  type="submit" class="btn btn-default" onclick="busca()">Buscar</button>
            
        </div>
      <hr>
     
      <table id="tabela1"  class="display" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>Nome Aluno</th>
              <th>Usuario r. Cadastro</th>
              <th>Data do Cadastro</th>
              <th>Tipo de Documento</th>
              <th>Quantidade de Imagens</th>
              <th>Imagens</th>
              <th>Metadados</th>
            </tr>
          </thead>
          <tbody id = "dados">
       
          </tbody>
        </table>
      
      <footer>
        <p>&copy; IFPB - João Pessoa</p>
      </footer>

    </div> <!-- /container -->
  
</body>
</html>