<?php
  session_start();
  if (!$_SESSION['logado'])
      header("Location:login.php");
    header("Content-Type: text/html; charset=utf-8",true);
    include "functionsPDO.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title><?php echo "Bem Vindo ".ucfirst($_SESSION["nome"])?></title>
  <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
  <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
    </style>
  <link href="bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
</head>
<body>

  
    <?php
    if (isset($_GET['cod'])) {
       $listar = documentoPorId($_GET['cod']);
    } 
    
    ?>
    <?php

    echo "<div class='container'>
      <h1>Metadados</h1>
      <table class='table table-bordered lead well'>
        <thead>
          <tr><th>Dados da Instituição</th></tr>
        </thead>
        <tbody>
          <tr><td>Nome:</td><td>".$listar[0]->instituicaonome."</td></tr>
        </tbody>
      </table>

      <table class='table table-bordered lead well'>
        <thead>
          <tr><th>Dados do Aluno</th></tr>
        </thead>
        <tbody>
          <tr><td>Nome:</td><td>".$listar[0]->alunonome."</td><td>Data Nascimento:</td><td>".$listar[0]->datanascimento."</td></tr>
          <tr><td>Matricula:</td><td>".$listar[0]->matricula."</td><td>CPF:</td><td>".$listar[0]->cpf."</td><td>RG:</td><td>".$listar[0]->rg."</td></tr>
          <tr><td>Nome da Mãe:</td><td>".$listar[0]->mae."o</td><td>Nome do Pai:</td><td>".$listar[0]->pai."</td></tr>
        </tbody>
      </table>

      <table class='table table-bordered lead well'>
        <thead>
          <tr><th>Dados do Curso do Aluno</th></tr>
        </thead>
        <tbody>
          <tr><td>Nome:</td><td>".$listar[0]->cursonome."</td><td>Nível:</td><td>".$listar[0]->nivel."</td></tr>
          <tr><td>Ano Início:</td><td>".$listar[0]->anoinicio."</td><td>Ano Fim:</td><td>".$listar[0]->anofim."</td><td>Situação:</td><td>".$listar[0]->situacao."</td></tr>
        </tbody>
      </table>

      <table class='table table-bordered lead well'>
        <thead>
          <tr><th>Dados dos Documentos Arquivados</th></tr>
        </thead>
        <tbody>
          <tr><td>Tipo:</td><td>".$listar[0]->tipodocumento."</td></tr>
          <tr><td>Nome:</td><td></td><td>Ano:</td><td>".$listar[0]->anodocumento."</td></tr>
        </tbody>
      </table>";

      ?>


      
      <hr>

      <footer>
        <p>&copy; IFPB - João Pessoa</p>
      </footer>

    </div> <!-- /container -->
  
</body>
</html>